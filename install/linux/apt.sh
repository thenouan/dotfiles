#!/bin/sh

DEVTOOLS=$HOME/.devtools
if [ ! -d $DEVTOOLS ]; then
    echo "Creating $DEVTOOLS"
    mkdir "$DEVTOOLS"
fi


echo -e "\n\nInstalling apt packages..."
echo "=========================="

repositories=(
    ppa:neovim-ppa/stable
)

for repository in "${repositories[@]}"; do
    sudo add-apt-repository -y "$repository"
done

# Update package list
sudo apt update


packages=(
    neovim
    git
    tmux
    zsh
    curl
)


for package in "${packages[@]}"; do
    if dpkg -s "$package"; then
        echo "$package already installed... Skipping."
    else
        echo "Installing $package."
        sudo apt install -y $package
    fi
done


# Install Oh My Zsh
export ZSH="$HOME/.oh-my-zsh"
if [ ! -d $ZSH ]; then
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
fi


# Packages that can't be installed using package manager
# fzf > command-line fuzzy finder.
#git clone --depth 1 https://github.com/junegunn/fzf.git $DEVTOOLS/fzf
#$DEVTOOLS/fzf/install

# z > an alias for "cd" command with memory.
#git clone --depth 1 https://github.com/rupa/z.git $DEVTOOLS/z
