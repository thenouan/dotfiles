#!/usr/bin/env bash

LINKS=$DOTFILES/links


link() {
  generic=$1
  specific="$1.$(uname | awk '{print tolower($0)}')"
  target=$2
  if [ -e $specific ]; then
    src=$specific
  else
    src=$generic
  fi

  if [ -e $target ]; then
    echo "~${target#$HOME} already exists... Skipping."
  else
    echo "Creating symlink for $target"
    ln -s $src $target
  fi
}



echo -e "\nCreating symlinks..."


linkables=$( find -H "$LINKS" -maxdepth 3 -name "*.symlink")
for file in $linkables; do
  target="$HOME/.$( basename $file '.symlink' )"
  link $file $target
done



echo -e "\n\nLinking to ~/.config"
if [ ! -d $HOME/.config ]; then
  echo "Creating ~/.config"
  mkdir -p $HOME/.config
fi

for config in $LINKS/config/*; do
  target=$HOME/.config/$( basename $config )
  link $config $target
done



# create vim symlinks

echo -e "\n\nCreating VIM symlinks"
VIMFILES=("$HOME/.vim:$LINKS/vim/.vim"
          "$HOME/.vimrc:$LINKS/vim/.vimrc")

for file in "${VIMFILES[@]}"; do
  KEY=${file%%:*}
  VALUE=${file#*:}

  link ${VALUE} ${KEY}
done
