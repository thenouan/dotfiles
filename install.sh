#/usr/bin/env bash

command_exists() {
  type "$1" > /dev/null 2>&1
}


# Absolute path to this script, e.g. /home/user/bin/foo.sh
INSTALLERPATH=$(perl -MCwd -e 'print Cwd::abs_path shift' $0)
# Absolute path this script is in, thus /home/user/bin
export DOTFILES_ABSOLUTE=$(dirname "$INSTALLERPATH")
export DOTFILES=$HOME/.dotfiles


echo "Installing dotfiles."

if [ -e $DOTFILES ]; then
  echo "~${DOTFILES#$HOME} already exists... Skipping."
else
  echo "Creating root dotfiles symlink to $DOTFILES"
  ln -s $DOTFILES_ABSOLUTE $DOTFILES
fi



case "$(uname)" in
  Darwin)
    echo "Running on OSX"
    source install/osx/brew.sh
    source install/osx/systemconfig.sh
    ;;
  Linux)
    echo "Running on Linux"
    source install/linux/apt.sh
    ;;
esac


# Creating symlinks
source install/link.sh

# Setting up Git
source install/git.sh

# Install vim plugins
source install/vim.sh


echo "Done! Reload your terminal."
